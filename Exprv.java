// Author: Quinn Hirt
// Course: CS 3180 Comparative Languages
// Professor: T. K. Prasad
// Assignment Title: Assignment 2
// Date: 3/22/2020

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;

public class Exprv
{
	public static ArrayList<String> instruction_stack = new ArrayList<String>();
	public static ArrayList<Number> calculation_stack = new ArrayList<Number>();

	public static void main(String[] args)
	{
		ingest(args[0]);
		evaluate(Integer.parseInt(args[1]), Double.parseDouble(args[2]), Integer.parseInt(args[3]));
	}

	// Reads lines from file
    public static void ingest(String filename)
    {
        try
        {
            File file = new File(filename);
            Scanner sfile = new Scanner(file);

            while(sfile.hasNextLine())
            {
                String line = sfile.nextLine();
                instruction_stack.add(line);
            }
        }
        catch(FileNotFoundException e)
        {
            System.out.println(e);
        }

    }

    public static void evaluate(int i, double a, int j)
    {
    	for(String s: instruction_stack)
        {
            if(s.compareTo("dadd") == 0)
            {
                calculation_stack.add(0, new Double((Double)calculation_stack.remove(0) + (Double)calculation_stack.remove(0)));
            }
            else if(s.compareTo("dmull") == 0)
            {
            	calculation_stack.add(0, new Double((Double)calculation_stack.remove(0) * (Double)calculation_stack.remove(0)));
            }
            else if(s.compareTo("iadd") == 0)
            {
                calculation_stack.add(0, new Integer((Integer)calculation_stack.remove(0) + (Integer)calculation_stack.remove(0)));
            }
            else if(s.compareTo("imull") == 0)
            {
                calculation_stack.add(0, new Integer((Integer)calculation_stack.remove(0) * (Integer)calculation_stack.remove(0)));
            }
            else if(s.compareTo("iload_0") == 0)
            {
                calculation_stack.add(0, i);
            }
            else if(s.compareTo("dload_1") == 0)
            {
                calculation_stack.add(0, a);
            }
            else if(s.compareTo("iload_3") == 0)
            {
                calculation_stack.add(0, j);
            }
            else if(s.compareTo("i2d") == 0)
            {
            	calculation_stack.add(0, new Double(calculation_stack.remove(0).doubleValue()));
            }
            else
            {
            	System.out.println("SOMETHING HORRIBLE HAS HAPPENED! OH DEAR GOD!");
            }
        }
        System.out.println(calculation_stack.get(0));
    }
}
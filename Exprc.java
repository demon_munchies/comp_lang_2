// Author: Quinn Hirt
// Course: CS 3180 Comparative Languages
// Professor: T. K. Prasad
// Assignment Title: Assignment 2
// Date: 3/22/2020

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;

// Base class for the AST
class Node
{
    String val;
    Node right;
    Node left;

    public Node(String val)
    {
        this.val = val;
    }

    public String eval()
    {
        return this.val;
    }

    @Override
    public String toString()
    {
        return this.val;
    }

}

// Extended class for * and + operations
class BinaryNode extends Node
{
    public BinaryNode(String val)
    {
        super(val);
    }

    @Override
    public String eval()
    {
        return "[ " + this.left.eval() + " " + this.val + " " + this.right.eval() + " ]";
    }
}

// Main class
public class Exprc
{
    // Stores the stream of characters from stdin
    public static ArrayList<Character> token_stream;
    public static ArrayList<String> instruction_stack = new ArrayList<String>();

    public static void main(String[] args)
    {
        ingest("expr.dat");
    }

    public static void traverse(Node root)
    {
        try
        {
            boolean[] boolArr = value((BinaryNode)root);
            // lhs is an int, rhs is a double
            if(boolArr[0] == true && boolArr[1] == false)
            {
                instruction_stack.add("d" + root.toString());
                if(root.left != null)
                {
                    instruction_stack.add("icoer");
                    traverse(root.left);
                }
                if(root.right != null)
                {
                    traverse(root.right);
                }
            }
            else if(boolArr[0] == false && boolArr[1] == true)
            {
                instruction_stack.add("d" + root.toString());
                if(root.left != null)
                {
                    traverse(root.left);
                }
                if(root.right != null)
                {
                    instruction_stack.add("icoer");
                    traverse(root.right);
                }
            }
            else if(boolArr[0] == true && boolArr[1] == true)
            {
                instruction_stack.add("i" + root.toString());
                if(root.left != null)
                {
                    traverse(root.left);
                }
                if(root.right != null)
                {
                    traverse(root.right);
                }
            }
            else if(boolArr[0] == false && boolArr[1] == false)
            {
                instruction_stack.add("d" + root.toString());
                if(root.left != null)
                {
                    traverse(root.left);
                }
                if(root.right != null)
                {
                    traverse(root.right);
                }
            }
            else
            {
                System.out.println("Horrible Error!");
                return;
            }
            return;
        }
        catch(ClassCastException e)
        {
            
        }
        catch(NullPointerException e)
        {

        }
        instruction_stack.add(root.toString());
        if(root.left != null)
        {
            traverse(root.left);
        }
        if(root.right != null)
        {
            traverse(root.right);
        }
    }

    public static void toByteCode(String filename)
    {
        try
        {
            File file = new File(filename);
            PrintWriter pw = new PrintWriter(file);

            for(int i = instruction_stack.size() - 1; i >= 0; i--)
            {
                if(instruction_stack.get(i).compareTo("d+") == 0)
                {
                    pw.println("dadd");
                }
                else if(instruction_stack.get(i).compareTo("d*") == 0)
                {
                    pw.println("dmull");
                }
                else if(instruction_stack.get(i).compareTo("i+") == 0)
                {
                    pw.println("iadd");
                }
                else if(instruction_stack.get(i).compareTo("i*") == 0)
                {
                    pw.println("imull");
                }
                else if(instruction_stack.get(i).compareTo("i") == 0)
                {
                    pw.println("iload_0");
                }
                else if(instruction_stack.get(i).compareTo("a") == 0)
                {
                    pw.println("dload_1");
                }
                else if(instruction_stack.get(i).compareTo("j") == 0)
                {
                    pw.println("iload_3");
                }
                else if(instruction_stack.get(i).compareTo("icoer") == 0)
                {
                    pw.println("i2d");
                }
            }
            pw.close();
        }
        catch(FileNotFoundException e)
        {
            System.out.println(e);
        }
    }

    // Reads lines from file
    public static void ingest(String filename)
    {
        try
        {
            File file = new File(filename);
            Scanner sfile = new Scanner(file);
            int count = 0;

            while(sfile.hasNextLine())
            {
                count++;
                String line = sfile.nextLine();
                token_stream = new ArrayList<Character>();
                for(char c: line.toCharArray())
                {
                    if(Character.compare(c, ' ') == 0)
                    {
                        continue;
                    }
                    else
                    {
                        token_stream.add(c);
                    }
                }
                try
                {
                    instruction_stack = new ArrayList<String>();
                    Node n = parse();
                    System.out.println(n.eval());
                    traverse(n);
                    toByteCode("" + count + ".jbc");
                }
                catch(NullPointerException e)
                {
                    // System.out.println("Invalid expression!");
                }
            }
        }
        catch(FileNotFoundException e)
        {
            System.out.println(e);
        }

    }

    public static boolean[] value(BinaryNode bn)
    {
        // true == int
        // false == double
        boolean[] boolArr = new boolean[2];
        try
        {
            if(bn.left.toString().compareTo("i") == 0 || bn.left.toString().compareTo("j") == 0)
            {
                // System.out.println("Found an int");
                boolArr[0] = true;
            }
            else if(bn.left.toString().compareTo("a") == 0)
            {
                // System.out.println("Found a double");
                boolArr[0] = false;
            }
            else if(bn.left.toString().compareTo("+") == 0 || bn.left.toString().compareTo("*") == 0)
            {
                boolean[] prev = value((BinaryNode)bn.left);
                boolArr[0] = prev[0] && prev[1];
            }
            else
            {
                System.out.println("Something went wrong!");
            }

            if(bn.right.toString().compareTo("i") == 0 || bn.right.toString().compareTo("j") == 0)
            {
                // System.out.println("Found an int");
                boolArr[1] = true;
            }
            else if(bn.right.toString().compareTo("a") == 0)
            {
                // System.out.println("Found a double");
                boolArr[1] = false;
            }
            else if(bn.right.toString().compareTo("+") == 0 || bn.right.toString().compareTo("*") == 0)
            {
                boolean[] prev = value((BinaryNode)bn.right);
                boolArr[1] = prev[0] && prev[1];
            }
            else
            {
                System.out.println("Something went wrong!");
            }

            return boolArr;
        }
        catch(NullPointerException e)
        {
            return null;
        }
        catch(ClassCastException e)
        {
            return null;
        }
    }

    // Takes stream of characters and constructs AST
    public static Node parse()
    {
        Node ast = null;
        // Continue until no more characters
        while(token_stream.size() != 0)
        {
            if(ast == null)
            {
                ast = expr();
            }
            // Characters are left
            if(token_stream.size() != 0)
            {
                // See determine if next chracater is a valid token for opertion
                // If it is, make the AST a subtree and continue to the right
                String temp = token_stream.remove(0).toString();
                if(temp.compareTo("+") == 0 || temp.compareTo("*") == 0)
                {
                    BinaryNode bn = new BinaryNode(temp);
                    bn.left = ast;
                    bn.right = expr();
                    ast = bn;
                }
                // Invalid token, return with invalid value
                else
                {
                    return null;
                }
            }
        }
        // Valid ast returned
        return ast;
    }

    // Determines if the next sequence of tokens is an expression
    public static Node expr()
    {
        // Catch IndexOutOfBounds due to ArrayList access
        // Terminate and return invalid value if exception
        try
        {
            // Expressions are terms
            // Sees if next sequence of tokens is a term
            Node term = term();

            // Expressions can also be term + term
            // Determine if binary node is needed for expression
            boolean hasPlus;
            try
            {
                hasPlus = token_stream.get(0).toString().compareTo("+") == 0;
            }
            catch(IndexOutOfBoundsException e)
            {
                hasPlus = false;
            }

            // Binary node is needed
            // Current tree becomes a subtree and continue to the right
            if(term != null && hasPlus)
            {
                token_stream.remove(0);
                BinaryNode bn = new BinaryNode("+");
                bn.left = term;
                bn.right = term();
                return bn;
            }
            // Unary node returned
            else
            {
                return term;
            }
        }
        // Catch an exception, return invalid value
        catch(IndexOutOfBoundsException e)
        {
            return null;
        }

    }
    
    // Determnines if the next sequence of tokens is a term
    public static Node term()
    {
        // Catch IndexOutOfBounds due to ArrayList access
        // Terminate and return invalid value if exception
        try
        {
            // Terms are factors
            Node factor = factor();

            // Terrms can also be factor * factor
            //Determine if binary node is needed for term
            boolean hasAsterix;
            try
            {
                hasAsterix = token_stream.get(0).toString().compareTo("*") == 0;
            }
            catch(IndexOutOfBoundsException e)
            {
                hasAsterix = false;
            }

            // Binary node is needed
            // Current tree becomes a subtree and continue to the right
            if(factor != null && hasAsterix)
            {
                token_stream.remove(0);
                BinaryNode bn = new BinaryNode("*");
                bn.left = factor;
                bn.right = factor();
                return bn;
            }
            //Unary node is returned
            else
            {
                return factor;
            }
        }
        // Catch an exception, return invalid value
        catch(IndexOutOfBoundsException e)
        {
            return null;
        }

    }

    // Determines if next sequence of tokens is a factor
    public static Node factor()
    {   
        // Catch IndexOutOfBounds due to ArrayList access
        // Terminate and return invalid value if exception
        try
        {
            // Determine next token
            String temp = token_stream.remove(0).toString();
            // Factors can be expressions surrounded by ()
            if(temp.compareTo("(") == 0)
            {
                // See if the next sequence of tokens is an expression
                Node nextExpr = expr();

                // Determine if next token is )
                boolean closingParen = token_stream.get(0).toString().compareTo(")") == 0;

                // Return the expression!
                if(nextExpr != null && closingParen)
                {   
                    token_stream.remove(0);
                    return nextExpr;
                }
                // Not an expression or no closing paren
                // Return invalid value
                else
                {
                    return null;
                }
            }
            // Factors can be terminal tokens 'a' or 'i' or 'j'
            else if(temp.compareTo("a") == 0 || 
                    temp.compareTo("i") == 0 ||
                    temp.compareTo("j") == 0)
            {
                return new Node(temp);
            }
            // Invalid token was reached
            else
            {
                return null;
            }
        }
        // Catch an exception, return invalid value
        catch(IndexOutOfBoundsException e)
        {
            return null;
        }
    }

}
